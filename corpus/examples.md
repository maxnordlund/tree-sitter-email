===============================================================================
Comments
===============================================================================

jane.doe@(comment)gmail.com
jane.doe@gmail.com(comment)
jane.doe(comment)@gmail.com
(comment)jane.doe@gmail.com

---

(root
  (address
    (local_part)
    (domain
      (comment)
      (name)
      (top)))
  (address
    (local_part)
    (domain
      (name)
      (top)
      (comment)))
  (address
    (local_part
      (comment))
    (domain
      (name)
      (top)))
  (address
    (local_part
      (comment))
    (domain
      (name)
      (top))))

===============================================================================
Quotes
===============================================================================

"simple"@email.net
"with some whitespace"@email.net

jane."the deer"@email.net
"the deer".doe@email.net
jane."the deer".doe@email.net

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top)))
  (address
    (local_part)
    (domain
      (name)
      (top)))
  (address
    (local_part)
    (domain
      (name)
      (top)))
  (address
    (local_part)
    (domain
      (name)
      (top)))
  (address
    (local_part)
    (domain
      (name)
      (top))))
