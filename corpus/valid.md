===============================================================================
Simple email address
===============================================================================

simple@example.com

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

===============================================================================
Very common, single dot seperates two names
===============================================================================

very.common@example.com

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

===============================================================================
Disposable style email with symbol
===============================================================================

disposable.style.email.with+symbol@example.com

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

===============================================================================
Email with hyphen
===============================================================================

other.email-with-hyphen@example.com

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

===============================================================================
Fully qualified domain
===============================================================================

fully-qualified-domain@example.com

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

===============================================================================
May go to user.name@example.com inbox depending on mail server
===============================================================================

user.name+tag+sorting@example.com

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

===============================================================================
One-letter local-part
===============================================================================

x@example.com

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

===============================================================================
Dashes in domain name
===============================================================================

example-indeed@strange-example.com

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))


===============================================================================
See the List of Internet top-level domains
===============================================================================

example@s.example

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

===============================================================================
Space between the quotes
===============================================================================

" "@example.org

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

===============================================================================
Quoted double dot
===============================================================================

"john..doe"@example.org

---

(root
  (address
    (local_part)
    (domain
      (name)
      (top))))

