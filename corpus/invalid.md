===============================================================================
Local domain name with no TLD (dotless) highly discouraged by ICANN
===============================================================================

admin@mailserver1

---

(ERROR (local_part) (ERROR))

===============================================================================
No @ character
===============================================================================

Abc.example.com

---

(ERROR
  (local_part))

===============================================================================
Only one @ is allowed outside quotation marks
===============================================================================

A@b@c@example.com

---

(root (address (local_part) (ERROR (top) (top)) (domain (name) (top))))

===============================================================================
None of the special characters in this local-part are allowed outside quotation marks
===============================================================================

a"b(c)d,e:f;g<h>i[j\k]l@example.com

---

(ERROR (ERROR))

===============================================================================
Quoted strings must be dot separated or the only element making up the local-part
===============================================================================

just"not"right@example.com

---

(root (ERROR) (address (local_part) (ERROR (top)) (domain (name) (top))))

===============================================================================
Spaces, quotes, and backslashes may only exist when within quoted strings and preceded by a backslash
===============================================================================

this is"not\allowed@example.com

---

(ERROR (ERROR (UNEXPECTED ' ') (top)))

===============================================================================
 Even if escaped (preceded by a backslash), spaces, quotes, and backslashes must still be contained by quotes
===============================================================================

this\ still\"not\\allowed@example.com

---

(root (address (local_part) (domain (name) (top))))

===============================================================================
Local part is longer than 64 character
===============================================================================

(Will actually pass due to tree-sitter not liking {1,63}. Probably turns into
63 nested optional.)
1234567890123456789012345678901234567890123456789012345678901234+x@example.com

---

(root (address (local_part (comment)) (domain (name) (top))))

===============================================================================
Double dot before @
===============================================================================

john..doe@example.com

---

(root (address (local_part (ERROR)) (domain (name) (top))))

===============================================================================
Double dot after @
===============================================================================

john.doe@example..com

---

(root (address (local_part) (domain (name) (ERROR) (top))))
