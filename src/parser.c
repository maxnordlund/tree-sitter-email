#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 9
#define STATE_COUNT 35
#define SYMBOL_COUNT 20
#define ALIAS_COUNT 0
#define TOKEN_COUNT 10
#define EXTERNAL_TOKEN_COUNT 0
#define MAX_ALIAS_SEQUENCE_LENGTH 0

enum {
  sym__newline = 1,
  anon_sym_AT = 2,
  sym_domain_labels = 3,
  sym_top_level_domain = 4,
  anon_sym_DOT = 5,
  sym__valid_characters = 6,
  anon_sym_DQUOTE = 7,
  aux_sym_SLASH_LPAREN_BSLASH_BSLASH_DOT_PIPE_LBRACK_CARET_DQUOTE_RBRACK_RPAREN_STAR_SLASH = 8,
  sym_comment = 9,
  sym_root = 10,
  sym_address = 11,
  sym_domain = 12,
  sym__domain = 13,
  sym_local_part = 14,
  sym__local_sub_part = 15,
  sym__quote = 16,
  aux_sym_root_repeat1 = 17,
  aux_sym_root_repeat2 = 18,
  aux_sym_local_part_repeat1 = 19,
};

static const char *ts_symbol_names[] = {
  [ts_builtin_sym_end] = "END",
  [sym__newline] = "_newline",
  [anon_sym_AT] = "@",
  [sym_domain_labels] = "name",
  [sym_top_level_domain] = "top",
  [anon_sym_DOT] = ".",
  [sym__valid_characters] = "_valid_characters",
  [anon_sym_DQUOTE] = "\"",
  [aux_sym_SLASH_LPAREN_BSLASH_BSLASH_DOT_PIPE_LBRACK_CARET_DQUOTE_RBRACK_RPAREN_STAR_SLASH] = "/(\\\\.|[^\"])*/",
  [sym_comment] = "comment",
  [sym_root] = "root",
  [sym_address] = "address",
  [sym_domain] = "domain",
  [sym__domain] = "_domain",
  [sym_local_part] = "local_part",
  [sym__local_sub_part] = "_local_sub_part",
  [sym__quote] = "_quote",
  [aux_sym_root_repeat1] = "root_repeat1",
  [aux_sym_root_repeat2] = "root_repeat2",
  [aux_sym_local_part_repeat1] = "local_part_repeat1",
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [sym__newline] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_AT] = {
    .visible = true,
    .named = false,
  },
  [sym_domain_labels] = {
    .visible = true,
    .named = true,
  },
  [sym_top_level_domain] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_DOT] = {
    .visible = true,
    .named = false,
  },
  [sym__valid_characters] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_DQUOTE] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_SLASH_LPAREN_BSLASH_BSLASH_DOT_PIPE_LBRACK_CARET_DQUOTE_RBRACK_RPAREN_STAR_SLASH] = {
    .visible = false,
    .named = false,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [sym_root] = {
    .visible = true,
    .named = true,
  },
  [sym_address] = {
    .visible = true,
    .named = true,
  },
  [sym_domain] = {
    .visible = true,
    .named = true,
  },
  [sym__domain] = {
    .visible = false,
    .named = true,
  },
  [sym_local_part] = {
    .visible = true,
    .named = true,
  },
  [sym__local_sub_part] = {
    .visible = false,
    .named = true,
  },
  [sym__quote] = {
    .visible = false,
    .named = true,
  },
  [aux_sym_root_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_root_repeat2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_local_part_repeat1] = {
    .visible = false,
    .named = false,
  },
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  switch (state) {
    case 0:
      if (lookahead == 0)
        ADVANCE(1);
      if (lookahead == '\n')
        ADVANCE(2);
      if (lookahead == '\r')
        ADVANCE(3);
      if (lookahead == '\"')
        ADVANCE(4);
      if (lookahead == '(')
        ADVANCE(5);
      if (lookahead == '.')
        ADVANCE(7);
      if (lookahead == '@')
        ADVANCE(8);
      if (lookahead == '\\')
        ADVANCE(9);
      if (('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(11);
      if (('!' <= lookahead && lookahead <= '\'') ||
          lookahead == '*' ||
          lookahead == '+' ||
          ('-' <= lookahead && lookahead <= '9') ||
          lookahead == '=' ||
          ('?' <= lookahead && lookahead <= 'Z') ||
          ('^' <= lookahead && lookahead <= '~'))
        ADVANCE(10);
      END_STATE();
    case 1:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 2:
      ACCEPT_TOKEN(sym__newline);
      END_STATE();
    case 3:
      if (lookahead == '\n')
        ADVANCE(2);
      END_STATE();
    case 4:
      ACCEPT_TOKEN(anon_sym_DQUOTE);
      END_STATE();
    case 5:
      if (lookahead == ')')
        ADVANCE(6);
      if (lookahead != 0)
        ADVANCE(5);
      END_STATE();
    case 6:
      ACCEPT_TOKEN(sym_comment);
      END_STATE();
    case 7:
      ACCEPT_TOKEN(anon_sym_DOT);
      END_STATE();
    case 8:
      ACCEPT_TOKEN(anon_sym_AT);
      END_STATE();
    case 9:
      if (lookahead != 0 &&
          lookahead != '\n')
        ADVANCE(10);
      END_STATE();
    case 10:
      ACCEPT_TOKEN(sym__valid_characters);
      if (lookahead == '\\')
        ADVANCE(9);
      if (lookahead == '!' ||
          ('#' <= lookahead && lookahead <= '\'') ||
          lookahead == '*' ||
          lookahead == '+' ||
          lookahead == '-' ||
          ('/' <= lookahead && lookahead <= '9') ||
          lookahead == '=' ||
          lookahead == '?' ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          ('^' <= lookahead && lookahead <= '~'))
        ADVANCE(10);
      END_STATE();
    case 11:
      ACCEPT_TOKEN(sym_top_level_domain);
      if (lookahead == '\\')
        ADVANCE(9);
      if (('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(11);
      if (lookahead == '!' ||
          ('#' <= lookahead && lookahead <= '\'') ||
          lookahead == '*' ||
          lookahead == '+' ||
          lookahead == '-' ||
          ('/' <= lookahead && lookahead <= '9') ||
          lookahead == '=' ||
          lookahead == '?' ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          ('^' <= lookahead && lookahead <= '~'))
        ADVANCE(10);
      END_STATE();
    case 12:
      if (lookahead == 0)
        ADVANCE(1);
      if (lookahead == '\n')
        ADVANCE(2);
      if (lookahead == '\r')
        ADVANCE(3);
      if (lookahead == '\"')
        ADVANCE(4);
      if (lookahead == '(')
        ADVANCE(5);
      if (lookahead == '\\')
        ADVANCE(9);
      if (('!' <= lookahead && lookahead <= '\'') ||
          lookahead == '*' ||
          lookahead == '+' ||
          lookahead == '-' ||
          ('/' <= lookahead && lookahead <= '9') ||
          lookahead == '=' ||
          lookahead == '?' ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          ('^' <= lookahead && lookahead <= '~'))
        ADVANCE(10);
      END_STATE();
    case 13:
      ACCEPT_TOKEN(aux_sym_SLASH_LPAREN_BSLASH_BSLASH_DOT_PIPE_LBRACK_CARET_DQUOTE_RBRACK_RPAREN_STAR_SLASH);
      if (lookahead == '\n')
        ADVANCE(14);
      if (lookahead == '\r')
        ADVANCE(17);
      if (lookahead == '\\')
        ADVANCE(15);
      if (lookahead != 0 &&
          lookahead != '\"')
        ADVANCE(16);
      END_STATE();
    case 14:
      ACCEPT_TOKEN(sym__newline);
      if (lookahead == '\\')
        ADVANCE(15);
      if (lookahead != 0 &&
          lookahead != '\"')
        ADVANCE(16);
      END_STATE();
    case 15:
      ACCEPT_TOKEN(aux_sym_SLASH_LPAREN_BSLASH_BSLASH_DOT_PIPE_LBRACK_CARET_DQUOTE_RBRACK_RPAREN_STAR_SLASH);
      if (lookahead == '\n')
        ADVANCE(16);
      if (lookahead == '\"')
        ADVANCE(16);
      if (lookahead == '\\')
        ADVANCE(15);
      if (lookahead != 0)
        ADVANCE(16);
      END_STATE();
    case 16:
      ACCEPT_TOKEN(aux_sym_SLASH_LPAREN_BSLASH_BSLASH_DOT_PIPE_LBRACK_CARET_DQUOTE_RBRACK_RPAREN_STAR_SLASH);
      if (lookahead == '\\')
        ADVANCE(15);
      if (lookahead != 0 &&
          lookahead != '\"')
        ADVANCE(16);
      END_STATE();
    case 17:
      ACCEPT_TOKEN(aux_sym_SLASH_LPAREN_BSLASH_BSLASH_DOT_PIPE_LBRACK_CARET_DQUOTE_RBRACK_RPAREN_STAR_SLASH);
      if (lookahead == '\n')
        ADVANCE(14);
      if (lookahead == '\\')
        ADVANCE(15);
      if (lookahead != 0 &&
          lookahead != '\"')
        ADVANCE(16);
      END_STATE();
    case 18:
      if (lookahead == 0)
        ADVANCE(1);
      if (lookahead == '\n')
        ADVANCE(2);
      if (lookahead == '\r')
        ADVANCE(3);
      END_STATE();
    case 19:
      if (lookahead == '\n')
        ADVANCE(2);
      if (lookahead == '\r')
        ADVANCE(3);
      if (lookahead == '(')
        ADVANCE(5);
      if (lookahead == '.')
        ADVANCE(7);
      if (lookahead == '@')
        ADVANCE(8);
      END_STATE();
    case 20:
      if (lookahead == '\n')
        ADVANCE(2);
      if (lookahead == '\r')
        ADVANCE(3);
      if (lookahead == '(')
        ADVANCE(5);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(21);
      END_STATE();
    case 21:
      if (lookahead == '-')
        ADVANCE(22);
      if (lookahead == '.')
        ADVANCE(24);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(21);
      END_STATE();
    case 22:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(23);
      END_STATE();
    case 23:
      if (lookahead == '.')
        ADVANCE(24);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(25);
      END_STATE();
    case 24:
      ACCEPT_TOKEN(sym_domain_labels);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(21);
      END_STATE();
    case 25:
      if (lookahead == '-')
        ADVANCE(22);
      if (lookahead == '.')
        ADVANCE(24);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(25);
      END_STATE();
    case 26:
      if (lookahead == '\n')
        ADVANCE(2);
      if (lookahead == '\r')
        ADVANCE(3);
      if (('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(27);
      END_STATE();
    case 27:
      ACCEPT_TOKEN(sym_top_level_domain);
      if (('a' <= lookahead && lookahead <= 'z'))
        ADVANCE(27);
      END_STATE();
    default:
      return false;
  }
}

static TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 12},
  [2] = {.lex_state = 13},
  [3] = {.lex_state = 12},
  [4] = {.lex_state = 18},
  [5] = {.lex_state = 0},
  [6] = {.lex_state = 19},
  [7] = {.lex_state = 19},
  [8] = {.lex_state = 12},
  [9] = {.lex_state = 12},
  [10] = {.lex_state = 12},
  [11] = {.lex_state = 19},
  [12] = {.lex_state = 12},
  [13] = {.lex_state = 20},
  [14] = {.lex_state = 12},
  [15] = {.lex_state = 19},
  [16] = {.lex_state = 19},
  [17] = {.lex_state = 12},
  [18] = {.lex_state = 12},
  [19] = {.lex_state = 12},
  [20] = {.lex_state = 19},
  [21] = {.lex_state = 19},
  [22] = {.lex_state = 19},
  [23] = {.lex_state = 12},
  [24] = {.lex_state = 26},
  [25] = {.lex_state = 20},
  [26] = {.lex_state = 0},
  [27] = {.lex_state = 12},
  [28] = {.lex_state = 19},
  [29] = {.lex_state = 19},
  [30] = {.lex_state = 19},
  [31] = {.lex_state = 12},
  [32] = {.lex_state = 12},
  [33] = {.lex_state = 0},
  [34] = {.lex_state = 0},
};

static uint16_t ts_parse_table[STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [sym__newline] = ACTIONS(1),
    [anon_sym_AT] = ACTIONS(1),
    [sym_top_level_domain] = ACTIONS(3),
    [anon_sym_DOT] = ACTIONS(1),
    [sym__valid_characters] = ACTIONS(3),
    [anon_sym_DQUOTE] = ACTIONS(1),
    [sym_comment] = ACTIONS(1),
  },
  [1] = {
    [sym_root] = STATE(4),
    [sym_address] = STATE(5),
    [sym_local_part] = STATE(6),
    [sym__local_sub_part] = STATE(7),
    [sym__quote] = STATE(7),
    [aux_sym_root_repeat1] = STATE(8),
    [aux_sym_root_repeat2] = STATE(9),
    [sym__newline] = ACTIONS(5),
    [sym__valid_characters] = ACTIONS(8),
    [anon_sym_DQUOTE] = ACTIONS(10),
    [sym_comment] = ACTIONS(12),
  },
  [2] = {
    [sym__newline] = ACTIONS(14),
    [aux_sym_SLASH_LPAREN_BSLASH_BSLASH_DOT_PIPE_LBRACK_CARET_DQUOTE_RBRACK_RPAREN_STAR_SLASH] = ACTIONS(16),
  },
  [3] = {
    [sym__local_sub_part] = STATE(11),
    [sym__quote] = STATE(11),
    [sym__newline] = ACTIONS(18),
    [sym__valid_characters] = ACTIONS(20),
    [anon_sym_DQUOTE] = ACTIONS(10),
  },
  [4] = {
    [ts_builtin_sym_end] = ACTIONS(22),
    [sym__newline] = ACTIONS(18),
  },
  [5] = {
    [aux_sym_root_repeat1] = STATE(12),
    [sym__newline] = ACTIONS(24),
  },
  [6] = {
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(27),
  },
  [7] = {
    [aux_sym_local_part_repeat1] = STATE(16),
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(29),
    [anon_sym_DOT] = ACTIONS(31),
    [sym_comment] = ACTIONS(33),
  },
  [8] = {
    [sym_address] = STATE(5),
    [sym_local_part] = STATE(6),
    [sym__local_sub_part] = STATE(7),
    [sym__quote] = STATE(7),
    [aux_sym_root_repeat1] = STATE(17),
    [aux_sym_root_repeat2] = STATE(18),
    [sym__newline] = ACTIONS(35),
    [sym__valid_characters] = ACTIONS(8),
    [anon_sym_DQUOTE] = ACTIONS(10),
    [sym_comment] = ACTIONS(12),
  },
  [9] = {
    [sym_address] = STATE(5),
    [sym_local_part] = STATE(6),
    [sym__local_sub_part] = STATE(7),
    [sym__quote] = STATE(7),
    [aux_sym_root_repeat2] = STATE(19),
    [ts_builtin_sym_end] = ACTIONS(38),
    [sym__newline] = ACTIONS(18),
    [sym__valid_characters] = ACTIONS(8),
    [anon_sym_DQUOTE] = ACTIONS(10),
    [sym_comment] = ACTIONS(12),
  },
  [10] = {
    [sym__newline] = ACTIONS(18),
    [anon_sym_DQUOTE] = ACTIONS(40),
  },
  [11] = {
    [aux_sym_local_part_repeat1] = STATE(22),
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(42),
    [anon_sym_DOT] = ACTIONS(31),
    [sym_comment] = ACTIONS(44),
  },
  [12] = {
    [aux_sym_root_repeat1] = STATE(23),
    [ts_builtin_sym_end] = ACTIONS(46),
    [sym__newline] = ACTIONS(48),
    [sym__valid_characters] = ACTIONS(46),
    [anon_sym_DQUOTE] = ACTIONS(46),
    [sym_comment] = ACTIONS(46),
  },
  [13] = {
    [sym_domain] = STATE(26),
    [sym__domain] = STATE(27),
    [sym__newline] = ACTIONS(18),
    [sym_domain_labels] = ACTIONS(51),
    [sym_comment] = ACTIONS(53),
  },
  [14] = {
    [sym__local_sub_part] = STATE(28),
    [sym__quote] = STATE(28),
    [sym__newline] = ACTIONS(18),
    [sym__valid_characters] = ACTIONS(55),
    [anon_sym_DQUOTE] = ACTIONS(10),
  },
  [15] = {
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(42),
  },
  [16] = {
    [aux_sym_local_part_repeat1] = STATE(29),
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(42),
    [anon_sym_DOT] = ACTIONS(31),
    [sym_comment] = ACTIONS(44),
  },
  [17] = {
    [aux_sym_root_repeat1] = STATE(17),
    [sym__newline] = ACTIONS(57),
    [sym__valid_characters] = ACTIONS(61),
    [anon_sym_DQUOTE] = ACTIONS(61),
    [sym_comment] = ACTIONS(61),
  },
  [18] = {
    [sym_address] = STATE(5),
    [sym_local_part] = STATE(6),
    [sym__local_sub_part] = STATE(7),
    [sym__quote] = STATE(7),
    [aux_sym_root_repeat2] = STATE(19),
    [ts_builtin_sym_end] = ACTIONS(63),
    [sym__newline] = ACTIONS(18),
    [sym__valid_characters] = ACTIONS(8),
    [anon_sym_DQUOTE] = ACTIONS(10),
    [sym_comment] = ACTIONS(12),
  },
  [19] = {
    [sym_address] = STATE(5),
    [sym_local_part] = STATE(6),
    [sym__local_sub_part] = STATE(7),
    [sym__quote] = STATE(7),
    [aux_sym_root_repeat2] = STATE(19),
    [ts_builtin_sym_end] = ACTIONS(46),
    [sym__newline] = ACTIONS(18),
    [sym__valid_characters] = ACTIONS(65),
    [anon_sym_DQUOTE] = ACTIONS(68),
    [sym_comment] = ACTIONS(71),
  },
  [20] = {
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(74),
    [anon_sym_DOT] = ACTIONS(74),
    [sym_comment] = ACTIONS(74),
  },
  [21] = {
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(76),
  },
  [22] = {
    [aux_sym_local_part_repeat1] = STATE(29),
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(76),
    [anon_sym_DOT] = ACTIONS(31),
    [sym_comment] = ACTIONS(78),
  },
  [23] = {
    [aux_sym_root_repeat1] = STATE(23),
    [ts_builtin_sym_end] = ACTIONS(61),
    [sym__newline] = ACTIONS(80),
    [sym__valid_characters] = ACTIONS(61),
    [anon_sym_DQUOTE] = ACTIONS(61),
    [sym_comment] = ACTIONS(61),
  },
  [24] = {
    [sym__newline] = ACTIONS(18),
    [sym_top_level_domain] = ACTIONS(84),
  },
  [25] = {
    [sym__domain] = STATE(32),
    [sym__newline] = ACTIONS(18),
    [sym_domain_labels] = ACTIONS(51),
  },
  [26] = {
    [sym__newline] = ACTIONS(86),
  },
  [27] = {
    [sym__newline] = ACTIONS(88),
    [sym_comment] = ACTIONS(91),
  },
  [28] = {
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(93),
    [anon_sym_DOT] = ACTIONS(93),
    [sym_comment] = ACTIONS(93),
  },
  [29] = {
    [aux_sym_local_part_repeat1] = STATE(29),
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(93),
    [anon_sym_DOT] = ACTIONS(95),
    [sym_comment] = ACTIONS(93),
  },
  [30] = {
    [sym__newline] = ACTIONS(18),
    [anon_sym_AT] = ACTIONS(98),
  },
  [31] = {
    [sym__newline] = ACTIONS(100),
    [sym_comment] = ACTIONS(100),
  },
  [32] = {
    [sym__newline] = ACTIONS(102),
    [sym_comment] = ACTIONS(105),
  },
  [33] = {
    [sym__newline] = ACTIONS(107),
  },
  [34] = {
    [sym__newline] = ACTIONS(109),
  },
};

static TSParseActionEntry ts_parse_actions[] = {
  [0] = {.count = 0, .reusable = false},
  [1] = {.count = 1, .reusable = true}, RECOVER(),
  [3] = {.count = 1, .reusable = false}, RECOVER(),
  [5] = {.count = 2, .reusable = true}, SHIFT(8), SHIFT_EXTRA(),
  [8] = {.count = 1, .reusable = true}, SHIFT(7),
  [10] = {.count = 1, .reusable = true}, SHIFT(2),
  [12] = {.count = 1, .reusable = true}, SHIFT(3),
  [14] = {.count = 1, .reusable = false}, SHIFT_EXTRA(),
  [16] = {.count = 1, .reusable = false}, SHIFT(10),
  [18] = {.count = 1, .reusable = true}, SHIFT_EXTRA(),
  [20] = {.count = 1, .reusable = true}, SHIFT(11),
  [22] = {.count = 1, .reusable = true}, ACCEPT_INPUT(),
  [24] = {.count = 2, .reusable = true}, SHIFT(12), SHIFT_EXTRA(),
  [27] = {.count = 1, .reusable = true}, SHIFT(13),
  [29] = {.count = 1, .reusable = true}, REDUCE(sym_local_part, 1),
  [31] = {.count = 1, .reusable = true}, SHIFT(14),
  [33] = {.count = 1, .reusable = true}, SHIFT(15),
  [35] = {.count = 2, .reusable = true}, SHIFT(17), SHIFT_EXTRA(),
  [38] = {.count = 1, .reusable = true}, REDUCE(sym_root, 1),
  [40] = {.count = 1, .reusable = true}, SHIFT(20),
  [42] = {.count = 1, .reusable = true}, REDUCE(sym_local_part, 2),
  [44] = {.count = 1, .reusable = true}, SHIFT(21),
  [46] = {.count = 1, .reusable = true}, REDUCE(aux_sym_root_repeat2, 2),
  [48] = {.count = 2, .reusable = true}, SHIFT(23), SHIFT_EXTRA(),
  [51] = {.count = 1, .reusable = true}, SHIFT(24),
  [53] = {.count = 1, .reusable = true}, SHIFT(25),
  [55] = {.count = 1, .reusable = true}, SHIFT(28),
  [57] = {.count = 3, .reusable = true}, REDUCE(aux_sym_root_repeat1, 2), SHIFT_REPEAT(17), SHIFT_EXTRA(),
  [61] = {.count = 1, .reusable = true}, REDUCE(aux_sym_root_repeat1, 2),
  [63] = {.count = 1, .reusable = true}, REDUCE(sym_root, 2),
  [65] = {.count = 2, .reusable = true}, REDUCE(aux_sym_root_repeat2, 2), SHIFT_REPEAT(7),
  [68] = {.count = 2, .reusable = true}, REDUCE(aux_sym_root_repeat2, 2), SHIFT_REPEAT(2),
  [71] = {.count = 2, .reusable = true}, REDUCE(aux_sym_root_repeat2, 2), SHIFT_REPEAT(3),
  [74] = {.count = 1, .reusable = true}, REDUCE(sym__quote, 3),
  [76] = {.count = 1, .reusable = true}, REDUCE(sym_local_part, 3),
  [78] = {.count = 1, .reusable = true}, SHIFT(30),
  [80] = {.count = 3, .reusable = true}, REDUCE(aux_sym_root_repeat1, 2), SHIFT_REPEAT(23), SHIFT_EXTRA(),
  [84] = {.count = 1, .reusable = true}, SHIFT(31),
  [86] = {.count = 1, .reusable = true}, REDUCE(sym_address, 3),
  [88] = {.count = 2, .reusable = true}, REDUCE(sym_domain, 1), SHIFT_EXTRA(),
  [91] = {.count = 1, .reusable = true}, SHIFT(33),
  [93] = {.count = 1, .reusable = true}, REDUCE(aux_sym_local_part_repeat1, 2),
  [95] = {.count = 2, .reusable = true}, REDUCE(aux_sym_local_part_repeat1, 2), SHIFT_REPEAT(14),
  [98] = {.count = 1, .reusable = true}, REDUCE(sym_local_part, 4),
  [100] = {.count = 1, .reusable = true}, REDUCE(sym__domain, 2),
  [102] = {.count = 2, .reusable = true}, REDUCE(sym_domain, 2), SHIFT_EXTRA(),
  [105] = {.count = 1, .reusable = true}, SHIFT(34),
  [107] = {.count = 1, .reusable = true}, REDUCE(sym_domain, 2),
  [109] = {.count = 1, .reusable = true}, REDUCE(sym_domain, 3),
};

#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_email_address() {
  static TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .symbol_metadata = ts_symbol_metadata,
    .parse_table = (const unsigned short *)ts_parse_table,
    .parse_actions = ts_parse_actions,
    .lex_modes = ts_lex_modes,
    .symbol_names = ts_symbol_names,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .lex_fn = ts_lex,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
  };
  return &language;
}
