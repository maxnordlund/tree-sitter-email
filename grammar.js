/* global grammar, seq, choice, repeat, repeat1, optional, prec, token, alias */
/* eslint no-unused-vars: off */
module.exports = grammar({
  name: "email_address",

  extras: $ => [$._newline],

  rules: {
    root: $ => seq(
      repeat($._newline),
      repeat1(seq(
        $.address,
        repeat1($._newline)
      ))
    ),

    _newline: $ => /\r?\n/,

    address: $ => seq(
      $.local_part,
      "@",
      $.domain,
    ),

    domain: $ => seq(
      optional($.comment),
      $._domain,
      optional($.comment),
    ),

    _domain: $ => seq(
      alias($.domain_labels, $.name),
      alias($.top_level_domain, $.top),
    ),

    domain_labels: $ => /(([a-z0-9]+(-[a-z0-9]+)?)+\.)+/,

    top_level_domain: $ => /[a-z]+/,

    local_part: $ => seq(
      optional($.comment),
      seq(
        $._local_sub_part,
        repeat(seq(".", $._local_sub_part)),
      ),
      optional($.comment),
    ),

    _local_sub_part: $ => choice(
      $._quote,
      $._valid_characters
    ),

    _valid_characters: $ => /(\\.|[-A-Za-z0-9!#$%&'*+/=?^_`{|}~])+/,

    _quote: $ => seq(
      '"',
      /(\\.|[^"])*/,
      '"',
    ),

    comment: $ => token(seq(
      "(",
      /[^)]*/,
      ")",
    )),
  },
})
