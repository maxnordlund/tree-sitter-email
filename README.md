# Email address parser
A [tree-sitter][1] parser for [email addresses][2] as specificed in
[RFC 3696][3].

The parser accepts syntactically valid email addresses, but does not check the
length requirements because that needs a hidden state inside the parser.

[1]: https://tree-sitter.github.io/tree-sitter/
[2]: https://en.wikipedia.org/wiki/Email_address
[3]: https://tools.ietf.org/html/rfc3696#section-3
